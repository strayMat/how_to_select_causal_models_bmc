# papers from jamia to cite/consider


- [ Applying machine learning to predict real-world individual treatment effects: insights from a virtual patient cohort ](https://pubmed.ncbi.nlm.nih.gov/31220274/)

 - [Healthcare utilization is a collider: an introduction to collider bias in EHR data reuse](https://academic.oup.com/jamia/article/30/5/971/7031302?searchresult=1)

 - [Estimating the causal effects of chronic disease combinations on 30-day hospital readmissions based on observational Medicaid data](https://academic.oup.com/jamia/article/25/6/670/4677331?searchresult=1#210323777)

 - [Explicit causal reasoning is needed to prevent prognostic models being victims of their own success](https://academic.oup.com/jamia/article/26/12/1675/5625126?searchresult=1)

 

 # condense:

 6000 avec les figures. donc environ 5200 sans. 

 On peut gagner sur le prior work.